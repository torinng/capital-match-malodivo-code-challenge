{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}

module District where 

import Data.Aeson
import Data.Maybe
import qualified Data.ByteString.Lazy.Char8 as BS
import GHC.Generics
import Data.List
import BillFund
import Debug.Trace

data RawDistrict = RawDistrict 
  { name :: String
  , availableFunds :: Double
  , categoryDefaultFunding :: [Fund]
  , billSpecificFunding :: [BillSpecificFunding]
  , caps :: [Fund]
  } deriving (Generic, Show)

rawDistrictName :: RawDistrict -> String 
rawDistrictName = name

instance FromJSON RawDistrict

data District = District 
  { districtName :: String
  , districtAvailableFunds :: Double
  , districtBillFunds :: [BillFund] 
  , districtCaps :: [Fund] }

data DistrictResult = DistrictResult 
  { name :: String
  , remainFunds :: Double 
  , billFunds :: [BillFundResult] 
  } deriving (Generic, Show)

instance ToJSON DistrictResult

-- allocate fund for specific bill in a district 
-- find by priority: specific > default 
getDistrictBillFund :: Bill -> RawDistrict -> BillFund
getDistrictBillFund b district = fromMaybe 
  (getCategoryDefaultBillFund b (categoryDefaultFunding district))
  (getSpecificBillFund b (billSpecificFunding district))


applyDistrictCap :: District -> District
applyDistrictCap d = District {
    districtName = districtName d
  , districtAvailableFunds = districtAvailableFunds d
  , districtBillFunds = applyCategoryCaps (districtBillFunds d) (districtCaps d)
  , districtCaps = districtCaps d
  }

-- allocate all bill funds for district
allocateDistrictFunds :: [Bill] -> RawDistrict -> District 
allocateDistrictFunds bills district = allocateDistrictFundByAvailableFunds $ applyDistrictCap District {
  districtName = rawDistrictName district,
  districtAvailableFunds = availableFunds district,
  districtBillFunds = map (`getDistrictBillFund` district) bills,
  districtCaps = caps district
}     

-- find and balance tax for specific bill from district bill funds
calculateBillFundByRatio :: Bill -> [BillFund] -> Double -> [BillFund] 
calculateBillFundByRatio b bills total = selected ++ tails 
  where 
    selected = map (\b' -> 
      changeBillFund b' (billFundAmount b' * billAmount b / total)
      (billFundAmount b' * billAmount b / total) True)
      $ filter (\b' -> billFundName b' == billName b) bills
    tails = filter (\b' -> billFundName b' /= billName b) bills 

-- allocate district funds by bill if bill amount <= total bill funds by category
allocateDistrictFundsByBill :: [District] -> Bill -> [District]
allocateDistrictFundsByBill districts b
  | districtBillFundTotal < billAmount b = districts 
  | otherwise = map (\d -> District {
    districtName = districtName d
  , districtAvailableFunds = districtAvailableFunds d
  , districtBillFunds = calculateBillFundByRatio b (districtBillFunds d) districtBillFundTotal
  , districtCaps = districtCaps d
  }) districts
  where
    districtFunds = concatMap (filter (\b' -> billFundName b' == billName b) . districtBillFunds) districts
    districtBillFundTotal = foldl (\res b' -> res + billFundAmount b') 0 districtFunds

allocateDistrictFundByAvailableFunds :: District -> District
allocateDistrictFundByAvailableFunds d 
    | availableFunds >= total = d
    | otherwise = District {
        districtName = districtName d
      , districtAvailableFunds = districtAvailableFunds d
      , districtBillFunds = 
          fixedBillFunds ++ map (\bf -> changeBillFundAmount bf (billFundAmount bf * availableFunds / total)) notFixedBillFunds
      , districtCaps = districtCaps d
    }
    where
      billFunds = districtBillFunds d
      fixedBillFunds = filter billFundIsFixed billFunds
      notFixedBillFunds = filter (not . billFundIsFixed) billFunds 
      availableFunds = foldl (\res bf -> res - billFundAmount bf) (districtAvailableFunds d) fixedBillFunds
      total = foldl (\t bf -> t + billFundAmount bf) 0.0 notFixedBillFunds

calculateDistrictBillFund :: [Bill] -> District -> DistrictResult 
calculateDistrictBillFund bills district = DistrictResult {
  name = districtName district,
  remainFunds = districtAvailableFunds district - totalAmount,
  billFunds = map toBillFundResult billResults
}
    where 
      billResults = districtBillFunds district
      totalAmount = foldl (\res b -> res + billFundAmount b) 0.0 billResults

calculateDistrictBillFunds :: [Bill] -> [District] -> [DistrictResult]
calculateDistrictBillFunds bills = map (calculateDistrictBillFund bills)

calculateRemainBill :: Bill -> [DistrictResult] -> BillFundResult
calculateRemainBill b districts = BillFundResult (billName b) (billCategory b) (billAmount b - districtAmount)
  where 
    districtBills = concatMap (filter (\b' -> billFundResultName b' == billName b) . billFunds) districts
    districtAmount = foldl (\res b' -> res + billFundResultAmount b') 0 districtBills

calculateRemainBills :: [Bill] -> [DistrictResult] -> [BillFundResult]
calculateRemainBills bills districtResults = map (`calculateRemainBill` districtResults)  bills

-- check if there is any fixed amount bill fund in districts
hasFixedBillFundAmount :: [District] -> Bool
hasFixedBillFundAmount = any billFundIsFixed . concatMap districtBillFunds
