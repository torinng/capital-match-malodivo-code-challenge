{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Malodivo where 

import Data.Aeson
import Data.Maybe
import qualified Data.ByteString.Lazy.Char8 as BS
import GHC.Generics
import Data.List
import BillFund
import District

data Input = Input
  { bills :: [Bill]
  , districts :: [RawDistrict]
  } deriving (Generic, Show)

instance FromJSON Input

data Output = Output 
  { remainBills :: [BillFundResult]
  , districtFunds :: [DistrictResult]
  } deriving (Generic, Show)

instance ToJSON Output


calculateOutput :: Input -> Output
calculateOutput input = Output {
  remainBills = remainBills,
  districtFunds = dictrictResults
}
  where 
    inputBills = bills input
    -- pick district bill funds from default and specific fund
    -- apply caps fund of district
    -- compare with available fund each district, balance funds if available funds < total funds of district
    bfDistricts = map (allocateDistrictFunds inputBills) (districts input)
    -- allocate funds by bills, balance district funds if total funds >= bill fund
    appliedBillDistricts = foldl allocateDistrictFundsByBill bfDistricts inputBills
    -- after allocated funds by bill, some fund can be odd because bill funds are small, it need to balance district funds again
    bfBalancedDistricts = if hasFixedBillFundAmount appliedBillDistricts 
      then map (allocateDistrictFundByAvailableFunds . applyDistrictCap) appliedBillDistricts
      else appliedBillDistricts
    -- calculate remain district funds
    dictrictResults = calculateDistrictBillFunds inputBills bfBalancedDistricts
    -- calculate remain bill funds
    remainBills = calculateRemainBills inputBills dictrictResults
