module Main where

import Malodivo
import qualified Data.ByteString.Lazy.Char8 as BS
import Data.Aeson

main :: IO ()
main = interact $ \input ->
  case decode (BS.pack input) :: Maybe Input of
    Just input -> BS.unpack . encode . calculateOutput $ input
    Nothing -> BS.unpack . encode $ "Error: could not understand input"
