{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}

module BillFund where 

import Data.Aeson
import Data.Maybe
import qualified Data.ByteString.Lazy.Char8 as BS
import GHC.Generics
import Data.List
import Debug.Trace

data Bill = Bill 
  { name :: String
  , category :: String
  , amount :: Double 
  } deriving (Generic, Show)

billName :: Bill -> String 
billName = name

billCategory :: Bill -> String 
billCategory = category

billAmount :: Bill -> Double
billAmount = amount


data Fund = Fund
  { category :: String
  , amount :: Double 
  } deriving (Generic, Show)

fundCategory :: Fund -> String 
fundCategory = category

fundAmount :: Fund -> Double 
fundAmount = amount 

data BillSpecificFunding = BillSpecificFunding 
  { bill :: String
  , amount :: Double 
  } deriving (Generic, Show)

specificFundAmount :: BillSpecificFunding -> Double 
specificFundAmount = amount 

instance FromJSON Bill
instance FromJSON Fund
instance FromJSON BillSpecificFunding
instance ToJSON Bill

type CappedFund = Fund

data BillFund = BillFund 
  { billFundName :: String
  , billFundCategory :: String
  , billFundAmount :: Double
  , billFundMaxAmount :: Double
  , billFundIsFixed :: Bool 
  } deriving (Show, Generic)


data BillFundResult = BillFundResult 
  { name :: String
  , category :: String
  , amount :: Double
  } deriving (Show, Generic)

instance ToJSON BillFundResult

billFundResultName :: BillFundResult -> String 
billFundResultName = name

billFundResultAmount :: BillFundResult -> Double 
billFundResultAmount = amount

toBillFundResult :: BillFund -> BillFundResult
toBillFundResult b = BillFundResult (billFundName b) (billFundCategory b) (billFundAmount b)

changeBillFundAmount :: BillFund -> Double -> BillFund
changeBillFundAmount bf amount = BillFund {
  billFundName = billFundName bf
  , billFundCategory = billFundCategory bf
  , billFundAmount = amount
  , billFundMaxAmount = billFundMaxAmount bf
  , billFundIsFixed = billFundIsFixed bf
}

changeBillFund :: BillFund -> Double -> Double -> Bool -> BillFund
changeBillFund bf amount maxAmount isFixed = BillFund {
  billFundName = billFundName bf
  , billFundCategory = billFundCategory bf
  , billFundAmount = amount
  , billFundMaxAmount = maxAmount
  , billFundIsFixed = isFixed
}

getSpecificBillFund :: Bill -> [BillSpecificFunding] -> Maybe BillFund
getSpecificBillFund b specificFunds = resultFunc <$> selected
  where 
    selected = find (\fund -> bill fund == billName b) specificFunds
    resultFunc fund = BillFund {
      billFundName = billName b,
      billFundCategory = billCategory b,
      billFundAmount = specificFundAmount fund,
      billFundMaxAmount = specificFundAmount fund,
      billFundIsFixed = False
    }

getCategoryDefaultBillFund :: Bill -> [Fund] -> BillFund
getCategoryDefaultBillFund b funds = BillFund {
    billFundName = billName b,
    billFundCategory = billCategory b,
    billFundAmount = amount,
    billFundMaxAmount = amount,
    billFundIsFixed = amount == 0
  } 
  where 
    selected = find (\fund -> fundCategory fund == billCategory b) funds
    amount = maybe 0 fundAmount selected

applyCategoryCap :: [BillFund] -> CappedFund -> [BillFund]
applyCategoryCap [] _ = []
applyCategoryCap bills cappedFund 
  | sumBillAmount <= cappedAmount = map (\b -> changeBillFundAmount b (billFundMaxAmount b)) bills
  | otherwise = map (\b -> 
      changeBillFund b 
        (if billFundIsFixed b 
          then billFundAmount b 
          else billFundMaxAmount b / sumBillAmount * cappedAmount)
          
        (if billFundMaxAmount b <= cappedAmount
          then billFundMaxAmount b else cappedAmount)
        (billFundIsFixed b)
    ) bills
  where 
    sumBillAmount = foldl (\res b -> if billFundIsFixed b 
                                      then res 
                                      else res + billFundMaxAmount b) 0 bills
    cappedAmount = foldl (\res b -> if not (billFundIsFixed b) 
                                    then res 
                                    else res - billFundMaxAmount b) (fundAmount cappedFund) bills 
    
applyCategoryCaps :: [BillFund] -> [CappedFund] -> [BillFund]
applyCategoryCaps [] _ = []
applyCategoryCaps bills [] = bills
applyCategoryCaps bills cappedFunds = concatMap applyCap groupBills
  where 
    groupBills = groupBy (\a b -> billFundCategory a == billFundCategory b) 
      $ sortBy (\a b -> compare (billFundCategory a) (billFundCategory b)) bills

    applyCap :: [BillFund] -> [BillFund]
    applyCap bs = maybe bs (applyCategoryCap bs) 
      $ find (\cfund -> fundCategory cfund == (billFundCategory . head) bs) cappedFunds
