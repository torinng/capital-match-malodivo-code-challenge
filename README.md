# Capital Match Hiring Coding Challenge

## Chalenge Information


## Requirement
+ GHC 8.2
+ Cabal 
+ Nix

## Build and Run 

### Build with Nix
```bash
nix-build ./release.nix
```

### Run 
```bash
./result/bin/malodivo < input.json
```

or output to JSON file:

```bash
./result/bin/malodivo < input.json > output.json
```

