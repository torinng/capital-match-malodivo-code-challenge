{-# LANGUAGE DuplicateRecordFields #-}

module Main where

import Test.Hspec
import Malodivo
import District
import BillFund
import Data.List
import Debug.Trace

billWallName :: String 
billWallName = "An Act to Construct the Great Wall of Malodivo"

sample1 :: Input
sample1 = Input {
  bills = [
    Bill billWallName "Defense" 10
  ],
  districts = [
    RawDistrict { 
      name = "Palolene", 
      availableFunds = 10000,  
      categoryDefaultFunding = [
        Fund "Defense" 1000,
        Fund "Welfare" 3000,
        Fund "Science" 5000
      ],
      billSpecificFunding = [
        BillSpecificFunding "An Act to Increase Retirement Benefits for Veterans" 500,
        BillSpecificFunding "An Act to Fund the Development of Longer-Lasting Paper" 750
      ],
      caps = [
        Fund "Defense" 3000,
        Fund "Welfare" 3199,
        Fund "Defense" 15000
      ]
    },
    RawDistrict { 
      name = "Southern Palolene", 
      availableFunds = 150000,  
      categoryDefaultFunding = [
        Fund "Defense" 500,
        Fund "Welfare" 5000,
        Fund "Science" 2000
      ],
      billSpecificFunding = [
      ],
      caps = [
        Fund "Defense" 10000,
        Fund "Welfare" 10000,
        Fund "Defense" 10000
      ]
    },
    RawDistrict { 
      name = "Lakos", 
      availableFunds = 400000,  
      categoryDefaultFunding = [
        Fund "Defense" 10000,
        Fund "Welfare" 1000,
        Fund "Science" 500
      ],
      billSpecificFunding = [
        BillSpecificFunding "An Act to Construct the Great Wall of Malodivo" 100000,
        BillSpecificFunding "An Act to Fund the Development of Longer-Lasting Paper" 750
      ],
      caps = [
        Fund "Defense" 30000,
        Fund "Welfare" 3000,
        Fund "Defense" 1000
      ]
    }
  ]
}


sample2 :: Input
sample2 = Input {
  bills = [
    Bill billWallName "Defense" 200000,
    Bill "An Act to Construct Shelters for the Homeless" "Welfare" 40000,
    Bill "An Act to Fund the Development of Longer-Lasting Paper" "Science" 14000,
    Bill "An Act to Increase Retirement Benefits for Veterans" "Welfare" 90000
  ],
  districts = [
    RawDistrict { 
      name = "Palolene", 
      availableFunds = 200000,  
      categoryDefaultFunding = [
        Fund "Defense" 1000,
        Fund "Welfare" 3000,
        Fund "Science" 5000
      ],
      billSpecificFunding = [
        BillSpecificFunding "An Act to Increase Retirement Benefits for Veterans" 500,
        BillSpecificFunding "An Act to Fund the Development of Longer-Lasting Paper" 7500
      ],
      caps = [
        Fund "Defense" 3000,
        Fund "Welfare" 3199,
        Fund "Defense" 15000
      ]
    }
  ]
}

main :: IO ()
main = hspec $
  describe "absolute" $ do
    it "Allocate output from input" $ do
      let result = calculateOutput sample1
      length (remainBills result) `shouldBe` 1
      length (districtFunds result) `shouldBe` 3
      let 
        funds = concatMap billFunds $ districtFunds result
        filteredFunds = filter (\bf -> billFundResultName bf == billWallName) funds
        in
          sum (map billFundResultAmount filteredFunds) `shouldBe` 10

    it "Successfully apply caps for district fund" $ do 
      let result = allocateDistrictFunds (bills sample2) (head $ districts sample2) 
      sort (map billFundAmount (districtBillFunds result)) `shouldBe` [457, 1000, 2742, 7500]

