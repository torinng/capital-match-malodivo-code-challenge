{ mkDerivation, aeson, base, bytestring, hspec, stdenv }:
mkDerivation {
  pname = "malodivo";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ aeson base bytestring ];
  testHaskellDepends = [ aeson base bytestring hspec ];
  license = stdenv.lib.licenses.bsd3;
}
